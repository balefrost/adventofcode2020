(ns adventofcode2020.day5
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

(defn parse-input-line [line]
  (-> line
    (str/replace "B" "1")
    (str/replace "F" "0")
    (str/replace "R" "1")
    (str/replace "L" "0")
    (parse-int 2)))

(def input (parse-input-lines parse-input-line))

(defn part1 []
  (reduce max input))

(defn part2 []
  (first
    (let [seatset (set input)
          upper (reduce max input)]
      (for [idx (range 1 (dec upper))
            :when (and
                    (seatset (inc idx))
                    (seatset (dec idx))
                    (not (seatset idx)))]
        idx))))

(defn -main []
  (println (part1))
  (println (part2)))