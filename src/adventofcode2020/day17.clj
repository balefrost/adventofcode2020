(ns adventofcode2020.day17
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

;(defn compute-bounds [cells]
;  (let [xs (map #(nth % 0) cells)
;        ys (map #(nth % 1) cells)
;        zs (map #(nth % 2) cells)]
;    (mapv
;      #(vector (apply min %) (apply max %))
;      [xs ys zs])))

(defn compute-bounds [cells]
  (let [ncoords (count (first cells))]
    (vec
      (for [idx (range ncoords)
            :let [values (map #(nth % idx) cells)]]
        [(apply min values) (apply max values)]))))

;(defn adjacent-coords [coord]
;  (let [[x y z] coord]
;    (for [xn (range (dec x) (+ 2 x))
;          yn (range (dec y) (+ 2 y))
;          zn (range (dec z) (+ 2 z))
;          :let [result [xn yn zn]]
;          :when (not= result coord)]
;      result)))

(defn adjacent-coords [coord]
  (letfn [(helper [coord]
            (if-let [[hd & tl] (seq coord)]
              (for [o (range (dec hd) (+ 2 hd))
                    ac (helper tl)]
                (vec (cons o ac)))
              [[]]))]
    (-> coord
        (helper)
        (set)
        (disj coord))))

;(defn coords-within-bounds [bounds]
;  (let [[[x1 x2] [y1 y2] [z1 z2]] bounds]
;    (for [x (range x1 (inc x2))
;          y (range y1 (inc y2))
;          z (range z1 (inc z2))]
;      [x y z])))

(defn coords-within-bounds [bounds]
  (if-let [[[low high] & tl] (seq bounds)]
    (for [o (range low (inc high))
          r (coords-within-bounds tl)]
      (vec (cons o r)))
    [[]]))

(defn grow-bounds [bounds margin]
  (mapv
    (fn [[b1 b2]]
      [(- b1 margin)
       (+ b2 margin)])
    bounds))

(defn parse-input-string [input-str]
  (let [lines (str/split-lines input-str)
        cells (into
                #{}
                (for [[line line-idx] (map vector lines (range))
                      [char char-idx] (map vector line (range))
                      :when (= \# char)]
                  [line-idx char-idx 0]))]
    cells))

(def input
  (parse-input-string (read-input)))

;(def input (parse-input-string ".#.\n..#\n###"))

(defn step [cells]
  (let [bounds (compute-bounds cells)
        cells (set
                (for [coord (coords-within-bounds (grow-bounds bounds 1))
                      :let [cnt (count (filter cells (adjacent-coords coord)))]
                      :when (if (cells coord)
                              (#{2 3} cnt)
                              (= 3 cnt))]
                  coord))]
    cells))

(defn part1 []
  (count (nth (iterate step input) 6)))

(defn part2 []
  (let [modified-input (set (map #(vec (concat % [0])) input))]
    (count (nth (iterate step modified-input) 6))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
