(ns adventofcode2020.day15
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

(def input (mapv parse-int (str/split "19,0,5,1,10,13" #",")))

(defn vec-tail [v n]
  (subvec v (- (count v) n)))

(defn speak-helper [number-history last-spoken turn-number]
  (lazy-seq
    (let [spoken (let [hist (get number-history last-spoken [])]
                   (if (> (count hist) 1)
                     (let [[a b] (vec-tail hist 2)]
                       (- b a))
                     0))]
      (cons
        spoken
        (speak-helper
          (update number-history spoken (fnil conj []) turn-number)
          spoken
          (inc turn-number))))))

(defn speak [init]
  (let [number-history (into {} (map #(vector %1 [%2]) init (range)))]
    (concat init (speak-helper number-history (last init) (count init)))))

(defn part1 []
  (nth (speak input) (dec 2020)))

(defn part2 []
  (nth (speak input) (dec 30000000)))

(defn -main []
  (println (str (part1)))
  (time
    (println (str (part2)))))
