(ns adventofcode2020.parser)

(def parse-any-char
  (fn [input]
    (if-let [[fch & input] (seq input)]
      [[fch input]]
      [])))

(defn parse-char [ch]
  (fn [input]
    (or
      (if-let [[fch & input] (seq input)]
        (if (= ch fch)
          [[fch input]]))
      [])))

(defn parse-char-pred [pred]
  (fn [input]
    (or
      (if-let [[fch & input] (seq input)]
        (if (pred fch)
          [[fch input]]))
      [])))

(defn parse-opt [parser]
  (fn [input]
    (concat
      (parser input)
      [[nil input]])))

(defn parse-str [str]
  (fn [input]
    (let [[hd tl] (split-at (count str) input)]
      (if (= hd (seq str))
        [[str tl]]
        []))))

(defn- parse-sequence-helper [input parsers]
  (if-let [[parser & parsers] (seq parsers)]
    (for [[parsed input] (parser input)
          [parsed-then input] (parse-sequence-helper input parsers)]
      [(cons parsed parsed-then) input])
    [[[] input]]))

(defn parse-sequence [& parsers]
  (if (some nil? parsers)
    (throw (IllegalArgumentException. "null parser")))
  (fn [input]
    (parse-sequence-helper input parsers)))

(defn parse-choice [& parsers]
  (fn [input]
    (mapcat
      #(% input)
      parsers)))

(defn parse-mapped [f parser]
  (fn [input]
    (for [[parsed input] (parser input)]
      [(f parsed) input])))

(defmacro parse-let [bindings & body]
  (if (not (vector? bindings)) (throw (IllegalArgumentException. "a vector for its binding")))
  (if (not (even? (count bindings))) (throw (IllegalArgumentException. "an even number of forms in binding vector")))
  (let [binding-forms (partition 2 bindings)]
    `(parse-mapped
       (fn [[~@(map first binding-forms)]]
         ~@body)
       (parse-sequence
         ~@(map second binding-forms)))))

(defn- parse-zero-or-more-helper [input p]
  (let [parse-results (p input)]
    (concat
      (for [[parse-result input] parse-results
            [more-result input] (parse-zero-or-more-helper input p)]
        [(vec (cons parse-result more-result)) input])
      [[[] input]])))

(defn parse-zero-or-more [p]
  (fn [input]
    (parse-zero-or-more-helper input p)))

(defn parse-one-or-more [p]
  (parse-let [hd p
              tl (parse-zero-or-more p)]
    (vec (cons hd tl))))

(def parse-ws
  (parse-zero-or-more (parse-char-pred #(Character/isWhitespace ^char %))))

(defmacro lazy-parser [& body]
  `(let [p# (delay ~@body)]
     (fn [input#]
       (@p# input#))))

(defn parse-eating-ws [parser]
  (parse-let [result parser
              _ parse-ws]
    result))

(defn parse-completely [parser input]
  (for [[result input] (parser input)
        :when (empty? input)]
    result))
