(ns adventofcode2020.day10
  (:use adventofcode2020.advent-util))

(def input
  (parse-input-lines parse-int))

;(def input (map parse-int (str/split-lines "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4")))

;(def input (map parse-int (str/split-lines "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3")))

(defn part1 []
  (let [sorted-adapters (sort input)
        sorted-ratings (concat [0] sorted-adapters [(+ 3 (last sorted-adapters))])
        differences (map - (drop 1 sorted-ratings) sorted-ratings)
        freqs (frequencies differences)]
    (* (get freqs 1 0)
       (get freqs 3 0))))

(defn tails [s]
  (lazy-seq
    (if (seq s)
      (cons s (tails (rest s)))
      [s])))

(defn get-valid-adapter-steps [adapters]
  (if (seq (rest adapters))
    (let [[a & adapters] (seq adapters)]
      (for [next (take-while #(<= (- % a) 3) adapters)]
        (drop-while #(< % next) adapters)))
    [[]]))

(defn part2 []
  (let [sorted-adapters (sort input)
        sorted-ratings (concat [0] sorted-adapters [(+ 3 (last sorted-adapters))])]
    (loop [adapter-tails (reverse (tails sorted-ratings))
           cache {[] 1}]
      (if-let [[t & adapter-tails] (seq adapter-tails)]
        (let [my-count (reduce + 0
                         (for [next-tail (get-valid-adapter-steps t)]
                           (get cache next-tail :error)))]
          (recur adapter-tails (assoc cache t my-count)))
        (get cache sorted-ratings)))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
