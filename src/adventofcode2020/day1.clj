(ns adventofcode2020.day1
  (:use adventofcode2020.advent-util))

(def input (set (parse-input-lines #(parse-int %))))

(defn part1 []
  (let [[result] (for [a input
                       :let [b (- 2020 a)]
                       :when (contains? input b)]
                   (* a b))]
    result))

(defn part2 []
  (let [[result] (for [a input
                       b input
                       :let [c (- 2020 a b)]
                       :when (contains? input c)]
                   (* a b c))]
    result))

(defn -main []
  (println (part1))
  (println (part2)))
