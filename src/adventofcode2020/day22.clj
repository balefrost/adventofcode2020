(ns adventofcode2020.day22
  (:use adventofcode2020.advent-util)
  (:use clojure.pprint)
  (:require [clojure.string :as str])
  (:import (clojure.lang PersistentQueue)))

(defn parse-input [input-str]
  (let [[lines1 lines2] (str/split input-str #"\n\n")
        deck1 (into PersistentQueue/EMPTY (map parse-int (drop 1 (str/split-lines lines1))))
        deck2 (into PersistentQueue/EMPTY (map parse-int (drop 1 (str/split-lines lines2))))]
    {:deck1 deck1
     :deck2 deck2}))

(def input
  (parse-input (read-input)))

;(def input
;  (parse-input "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10"))

(defn play-round-pt1 [state]
  (let [{:keys [deck1 deck2]} state]
    (if (or (empty? deck1) (empty? deck2))
      state
      (let [card1 (peek deck1)
            card2 (peek deck2)
            deck1 (pop deck1)
            deck2 (pop deck2)]
        (if (> card1 card2)
          {:deck1 (conj deck1 card1 card2)
           :deck2 deck2}
          {:deck1 deck1
           :deck2 (conj deck2 card2 card1)})))))

(defn play-game-pt1 [init-state]
  (last (iterate-until-stable play-round-pt1 init-state)))

(declare play-game-pt2)

(defn play-round-pt2 [state]
  (let [{:keys [deck1 deck2 prior-rounds winner]} state]
    (cond
      winner
      state

      (contains? prior-rounds [deck1 deck2])
      (assoc state :winner :player1)

      (empty? deck1)
      (assoc state :winner :player2)

      (empty? deck2)
      (assoc state :winner :player1)

      :else
      (let [card1 (peek deck1)
            card2 (peek deck2)
            udeck1 (pop deck1)
            udeck2 (pop deck2)
            winner (cond
                     (and (>= (count udeck1) card1)
                          (>= (count udeck2) card2))
                     (let [recursive-result (play-game-pt2
                                              {:deck1 (into PersistentQueue/EMPTY (take card1 udeck1))
                                               :deck2 (into PersistentQueue/EMPTY (take card2 udeck2))})]
                       (:winner recursive-result))

                     (> card1 card2)
                     :player1

                     :else
                     :player2)]
        (case winner
          :player1
          (assoc state
            :deck1 (conj udeck1 card1 card2)
            :deck2 udeck2
            :prior-rounds (conj prior-rounds [deck1 deck2]))

          :player2
          (assoc state
            :deck1 udeck1
            :deck2 (conj udeck2 card2 card1)
            :prior-rounds (conj prior-rounds [deck1 deck2])))))))


(defn play-game-pt2 [init-state]
  (last (iterate-until-stable play-round-pt2 (assoc init-state :prior-rounds #{} :winner nil))))

;(defmethod print-method PersistentQueue [x w] (print-method (vec x) w))
;
(defn score-deck [deck]
  (reduce
    + 0
    (map
      *
      (reverse deck)
      (iterate inc 1))))


(defn score-game [state]
  (let [decks (map (partial get state) [:deck1 :deck2])
        scores (map score-deck decks)]
    (apply max scores)))

(defn part1 []
  (score-game (play-game-pt1 input)))

(defn part2 []
  (score-game (play-game-pt2 input)))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
