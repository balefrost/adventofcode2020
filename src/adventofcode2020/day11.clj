(ns adventofcode2020.day11
  (:use adventofcode2020.advent-util))

(def input
  (vec (parse-input-lines vec)))

;(def input (mapv vec (str/split-lines "L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL")))

;(def input (mapv vec (str/split-lines ".......#.\n...#.....\n.#.......\n.........\n..#L....#\n....#....\n.........\n#........\n...#.....")))

(def adjacent-dirs
  [[-1 -1]
   [-1 0]
   [-1 1]
   [0 -1]
   [0 1]
   [1 -1]
   [1 0]
   [1 1]])

(defn step [seats adjacency-lookup update-fn]
  (vec (for [y (range (count seats))
             :let [row (nth seats y)]]
         (vec (for [x (range (count row))
                    :let [seat (nth row x)
                          adjacent-values (map #(get-in seats %) (get-in adjacency-lookup [y x]))
                          adjacent-occupied (count (filter #{\#} adjacent-values))]]
                (update-fn seat adjacent-occupied))))))

(defn make-location-lookup [cast-fn seats]
  (vec (for [y (range (count seats))
             :let [row (nth seats y)]]
         (vec (for [x (range (count row))]
                (for [[dy dx] adjacent-dirs]
                  (cast-fn seats y x dy dx)))))))

(defn update-part1 [seat adjacent-occupied]
  (cond
    (and (= \L seat) (zero? adjacent-occupied)) \#
    (and (= \# seat) (>= adjacent-occupied 4)) \L
    :else seat))

(defn part1 []
  (let [location-lookup (make-location-lookup (fn [_ y x dy dx] [(+ y dy) (+ x dx)]) input)
        step-part1 (fn [seats] (step seats location-lookup update-part1))
        final-config (last (iterate-until-stable step-part1 input))]
    (count (filter #{\#} (flatten final-config)))))

(defn cast-ray [seats y x dy dx]
  (first
    (for [location (drop 1 (iterate #(map + % [dy dx]) [y x]))
          :let [value (get-in seats (vec location))]
          :when (not= value \.)]
      location)))

(defn update-part2 [seat adjacent-occupied]
  (cond
    (and (= \L seat) (zero? adjacent-occupied)) \#
    (and (= \# seat) (>= adjacent-occupied 5)) \L
    :else seat))

(defn part2 []
  (let [location-lookup (make-location-lookup cast-ray input)
        step-part2 (fn [seats] (step seats location-lookup update-part2))
        final-config (last (iterate-until-stable step-part2 input))]
    (count (filter #{\#} (flatten final-config)))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
