(ns adventofcode2020.day8
  (:use adventofcode2020.advent-util))

(defn parse-line [line]
  (let [[_ instr sign value] (re-find #"(\w+) ([+-])(\d+)" line)]
    {:instr (keyword instr)
     :value ((resolve (symbol sign)) (parse-int value))}))

(def input
  (vec (parse-input-lines parse-line)))

(defn step-machine [current-state program]
  (let [pc (:pc current-state)
        {:keys [instr value]} (nth program pc)]
    (case instr
      :nop (update current-state :pc inc)
      :acc (-> current-state
               (update :pc inc)
               (update :accum + value))
      :jmp (update current-state :pc + value))))

(defn run-detecting-termination [program]
  (letfn [(helper [current-state seen-pcs]
            (lazy-seq
              (cons
                current-state
                (let [pc (:pc current-state)]
                  (cond
                    (>= pc (count program)) [(assoc current-state :state :terminated-success)]
                    (seen-pcs pc) [(assoc current-state :state :terminated-infinite-loop)]
                    :else (let [updated-state (step-machine current-state program)]
                            (helper updated-state (conj seen-pcs pc))))))))]
    (helper {:pc 0 :accum 0 :state :running} #{})))

(defn part1 []
  (:accum (last (run-detecting-termination input))))

(defn part2 []
  (let [possible-programs (for [idx (range (count input))
                                :let [instr (:instr (nth input idx))]
                                :when (#{:jmp, :nop} instr)
                                :let [replacement (case instr
                                                    :jmp :nop
                                                    :nop :jmp)]]
                            (assoc-in input [idx :instr] replacement))]
    (first
      (for [result (pmap
                     #(last (run-detecting-termination %1))
                     possible-programs)
            :when (= (:state result) :terminated-success)]
        (:accum result)))))


(defn -main []
  (println (part1))
  (println (part2)))
