(ns adventofcode2020.day2
  (:use adventofcode2020.advent-util))

(def line-regex #"^(\d+)-(\d+) (.): (.*)$")

(defn parse-line [line]
  (let [[[_ low high ch input]] (re-seq line-regex line)]
    {:low   (parse-int low)
     :high  (parse-int high)
     :ch    (nth ch 0)
     :input input}))

(def input
  (parse-input-lines parse-line))

(defn is-valid-password-part1 [entry]
  (let [{:keys [low high ch input]} entry
        num-matches (count (for [c input
                                 :when (= c ch)]
                             c))]
    (<= low num-matches high)))

(defn is-valid-password-part2 [entry]
  (let [{:keys [low high ch input]} entry
        num-matches (count (for [position [low high]
                                 :when (= ch (nth input (dec position)))]
                             position))]
    (= 1 num-matches)))

(defn solve [is-valid-password]
  (let [good-input (filter is-valid-password input)]
    (count good-input)))

(defn part1 []
  (solve is-valid-password-part1))

(defn part2 []
  (solve is-valid-password-part2))

(defn -main []
  (println (part1))
  (println (part2)))
