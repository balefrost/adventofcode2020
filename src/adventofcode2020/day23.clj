(ns adventofcode2020.day23
  (:use adventofcode2020.advent-util)
  (:use clojure.pprint)
  (:require [clojure.string :as str]))

(defn parse-input [input-str]
  (mapv (comp parse-int str) input-str))

(def input
  (parse-input "624397158"))

;(def input
;  (parse-input "389125467"))

(defn in-range [low high x]
  (let [span (- high low)]
    (+ low (mod (- x low) span))))

(defn next-cup-seq
  ([nexts]
   (next-cup-seq nexts (first nexts)))
  ([nexts init]
   (letfn [(helper [init]
             (lazy-seq
               (cons init (helper (get nexts init)))))]
     (take (dec (count nexts)) (helper init)))))

(defn play-round-pt2 [nexts]
  (let [curr-number (first nexts)
        max-cup (count nexts)
        [_ a b c d] (next-cup-seq nexts)
        taken #{a b c}
        insertion-number (first (filter (comp not taken) (drop 1 (iterate #(in-range 1 max-cup (dec %)) curr-number))))
        after-insertion-number (get nexts insertion-number)]
    (assoc nexts 0 d
                 curr-number d
                 insertion-number a
                 c after-insertion-number)))

(defn make-nexts [all-numbers]
  (let [nexts (reduce
                (fn [v [a b]]
                  (assoc v a b))
                (vec (repeat (inc (count all-numbers)) 0))
                (partition 2 1 (concat all-numbers [(first all-numbers)])))
        nexts (assoc nexts 0 (first all-numbers))]
    nexts))


(defn part1 []
  (let [nexts (make-nexts input)
        final-state (nth
                      (iterate play-round-pt2 nexts)
                      100)]
    (str/join
      (drop 1 (next-cup-seq
                final-state
                1)))))

; 149245887792 too low

(defn part2 []
  (let [nexts (make-nexts (concat input (range 10 1000001)))
        final-state (nth
                      (iterate play-round-pt2 nexts)
                      10000000)
        final-seq (next-cup-seq
                    final-state
                    1)
        [_ a b] final-seq]
    (* a b)))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
