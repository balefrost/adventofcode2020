(ns adventofcode2020.day7
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

(defn parse-line [line]
  (let [[container allcontained] (str/split line #"\s+bags contain\s+")
        singlecontained (re-seq #"(\d+) (.*?) bags?" allcontained)
        parsed-contained (for [[_ count type] singlecontained]
                           {:type  type
                            :count (parse-int count)})]
    {:container container
     :contained parsed-contained}))

(def input
  (parse-input-lines parse-line))

;(def input
;  (map parse-line (str/split-lines "light red bags contain 1 bright white bag, 2 muted yellow bags.\ndark orange bags contain 3 bright white bags, 4 muted yellow bags.\nbright white bags contain 1 shiny gold bag.\nmuted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\nshiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\ndark olive bags contain 3 faded blue bags, 4 dotted black bags.\nvibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\nfaded blue bags contain no other bags.\ndotted black bags contain no other bags.")))

;(def input
;  (map parse-line (str/split-lines "shiny gold bags contain 2 dark red bags.\ndark red bags contain 2 dark orange bags.\ndark orange bags contain 2 dark yellow bags.\ndark yellow bags contain 2 dark green bags.\ndark green bags contain 2 dark blue bags.\ndark blue bags contain 2 dark violet bags.\ndark violet bags contain no other bags.")))

(defn part1 []
  (let [possible-containers (apply merge-with into (for [{:keys [container contained]} input
                                                         {:keys [type]} contained]
                                                     {type #{container}}))]
    (count (filter (comp not (partial = "shiny gold")) (trans-closure possible-containers ["shiny gold"])))))

(defn part2 []
  ; This could be optimized. Right now, it adds bags to the bag pile one at a time, and each of these bags is opened
  ; one-at-a-time. This means that if we end up at some point with 10 red bags, there will be 10 instances
  ; of "red bag" in the backlog. We could instead open all 10 red bags all at once. Then the backlog would be
  ; a map instead of a list.
  ;
  ; Another optimization is, on the assumption that input describes an acyclic graph, to compute a topological
  ; sort of the bag types. That way, we could delay opening any bags of a given type until we've "discovered"
  ; all bags of that kind. This would change the runtime to be O(n), where n is the number of bag types that
  ; are ultimately contained within the starting bag.
  (let [contents (apply conj {} (for [{:keys [container contained]} input]
                                  [container (apply conj {} (for [{:keys [type count]} contained]
                                                              [type count]))]))]
    (loop [total-bags -1
           unopened-bags ["shiny gold"]]
      (if-let [[b & bgs] (seq unopened-bags)]
        (let [new-bags (for [[type count] (seq (contents b))
                             elem (repeat count type)]
                         elem)]
          (recur (inc total-bags) (concat new-bags bgs)))
        total-bags))))


(defn -main []
  (println (part1))
  (println (part2)))