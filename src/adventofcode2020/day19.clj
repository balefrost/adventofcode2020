(ns adventofcode2020.day19
  (:use adventofcode2020.advent-util)
  (:use adventofcode2020.parser)
  (:require [clojure.string :as str]))

(def parse-digit
  (parse-char-pred #(Character/isDigit ^char %)))

(def parse-number
  (parse-eating-ws
    (parse-mapped
      #(parse-int (str/join %))
      (parse-one-or-more
        parse-digit))))

(def parse-literal-char
  (parse-eating-ws
    (parse-let [_ (parse-char \")
                ch parse-any-char
                _ (parse-char \")]
      ch)))

(defn parse-one-or-more-with-separator [value-parser separator-parser]
  (parse-let [init-results value-parser
              rest-results (parse-zero-or-more
                             (parse-let [_ separator-parser
                                         result value-parser]
                               result))]
    (vec (cons init-results rest-results))))

(def parse-choices
  (parse-one-or-more-with-separator
    (parse-one-or-more
      parse-number)
    (parse-eating-ws
      (parse-char \|))))

(def parse-rule
  (parse-let [number (parse-eating-ws parse-number)
              _ (parse-eating-ws (parse-char \:))
              pattern (parse-eating-ws
                        (parse-choice
                          parse-choices
                          parse-literal-char))]
    {:number  number
     :pattern pattern}))

(defn parse-rules [rules-strs]
  (into
    {}
    (for [rule rules-strs]
      (let [{:keys [number pattern]} (first (parse-completely parse-rule rule))]
        [number pattern]))))

(defn parse-input [input-str]
  (let [[rules inputs] (str/split input-str #"\n\n")
        rules (str/split-lines rules)
        rules (parse-rules rules)
        inputs (str/split-lines inputs)]
    {:rules  rules
     :inputs inputs}))

(def input
  (parse-input (read-input)))

;(def input
;  (parse-input "42: 9 14 | 10 1\n9: 14 27 | 1 26\n10: 23 14 | 28 1\n1: \"a\"\n11: 42 31\n5: 1 14 | 15 1\n19: 14 1 | 14 14\n12: 24 14 | 19 1\n16: 15 1 | 14 14\n31: 14 17 | 1 13\n6: 14 14 | 1 14\n2: 1 24 | 14 4\n0: 8 11\n13: 14 3 | 1 12\n15: 1 | 14\n17: 14 2 | 1 7\n23: 25 1 | 22 14\n28: 16 1\n4: 1 1\n20: 14 14 | 1 15\n3: 5 14 | 16 1\n27: 1 6 | 14 18\n14: \"b\"\n21: 14 1 | 1 14\n25: 1 1 | 1 14\n22: 14 14\n8: 42\n26: 14 22 | 1 20\n18: 15 15\n7: 14 5 | 1 21\n24: 14 1\n\nabbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa\nbbabbbbaabaabba\nbabbbbaabbbbbabbbbbbaabaaabaaa\naaabbbbbbaaaabaababaabababbabaaabbababababaaa\nbbbbbbbaaaabbbbaaabbabaaa\nbbbababbbbaaaaaaaabbababaaababaabab\nababaaaaaabaaab\nababaaaaabbbaba\nbaabbaaaabbaaaababbaababb\nabbbbabbbbaaaababbbbbbaaaababb\naaaaabbaabaaaaababaa\naaaabbaaaabbaaa\naaaabbaabbaaaaaaabbbabbbaaabbaabaaa\nbabaaabbbaaabaababbaabababaaab\naabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"))

(defn convert-pattern [lookup number pattern]
  (cond
    (char? pattern)
    (parse-mapped
      (constantly number)
      (parse-char pattern))

    (vector? pattern)
    (parse-mapped
      (fn [parsed]
        {:rule number :children parsed})
      (apply
        parse-choice
        (for [alternative pattern]
          (apply
            parse-sequence
            (for [ruleno alternative]
              (lazy-parser (get @lookup ruleno)))))))))

(defn convert-rules [rules]
  (let [lookup (atom nil)
        converted-rules (into {}
                              (map
                                (fn [[number pattern]] [number (convert-pattern lookup number pattern)])
                                rules))]
    (reset! lookup converted-rules)
    (get @lookup 0)))


(defn part1 []
  (let [rule0 (convert-rules (:rules input))]
    (count (filter #(first (parse-completely rule0 %)) (:inputs input)))))

(def newrules "8: 42 | 42 8\n11: 42 31 | 42 11 31")

(defn part2 []
  (let [new-parsed-rules (parse-rules (str/split-lines newrules))
        merged-rules (merge (:rules input) new-parsed-rules)
        rule0 (convert-rules merged-rules)]
    (count (filter #(first (parse-completely rule0 %)) (:inputs input)))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
