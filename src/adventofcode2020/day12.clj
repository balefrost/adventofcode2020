(ns adventofcode2020.day12
  (:use adventofcode2020.advent-util))

(defn parse-line [line]
  (let [[_ cmd amount] (re-matches #"(\w)(\d+)" line)]
    {:cmd cmd :amt (parse-int amount)}))

(def input
  (parse-input-lines parse-line))

;(def input (map parse-line (str/split-lines "F10\nN3\nF7\nR90\nF11")))

;(def input (map parse-line (str/split-lines "R270\nF10")))

(defn make-rot-mat [cw-steps]
  (let [r (* cw-steps Math/PI 0.5)
        s (Math/round (Math/sin r))
        c (Math/round (Math/cos r))]
    [[c (- s)]
     [s c]]))

(def dir-mats
  (mapv make-rot-mat (range 4)))

(defn mat-mult [mat v]
  (mapv #(reduce + (map * v %)) mat))

(defn rotate-pos [steps-ccw pos]
  (let [mat (get dir-mats (mod steps-ccw 4))]
    (mat-mult mat pos)))

(defn move-ship [dir state]
  (update state :pos #(mapv + dir %)))

(defn turn-ship [degs-ccw state]
  (let [steps-ccw (/ degs-ccw 90)]
    (update state :wp #(rotate-pos steps-ccw %))))

(defn forward [dist state]
  (let [dir (:wp state)
        dp (mapv (partial * dist) dir)]
    (move-ship dp state)))

(defn move-wp [dir state]
  (update state :wp #(mapv + dir %)))

(defn sail [state step cmds]
  (reductions
    step
    state
    cmds))

(defn solve [state step cmds]
  (let [final-state (last (sail state step cmds))
        final-pos (:pos final-state)
        offsets (map (comp abs-value -) final-pos [0 0])]
    (reduce + offsets)))

(def initial-state-part1 {:pos [0 0] :wp [1 0]})

(defn step-part1 [state {:keys [cmd amt]}]
  (case cmd
    "N" (move-ship [0 amt] state)
    "S" (move-ship [0 (- amt)] state)
    "E" (move-ship [amt 0] state)
    "W" (move-ship [(- amt) 0] state)
    "L" (turn-ship amt state)
    "R" (turn-ship (- amt) state)
    "F" (forward amt state)))

(defn part1 []
  (solve initial-state-part1 step-part1 input))

(def initial-state-part2 {:pos [0 0] :wp [10 1]})

(defn step-part2 [state {:keys [cmd amt]}]
  (case cmd
    "N" (move-wp [0 amt] state)
    "S" (move-wp [0 (- amt)] state)
    "E" (move-wp [amt 0] state)
    "W" (move-wp [(- amt) 0] state)
    "L" (turn-ship amt state)
    "R" (turn-ship (- amt) state)
    "F" (forward amt state)))

(defn part2 []
  (solve initial-state-part2 step-part2 input))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
