(ns adventofcode2020.day16
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]
            [clojure.set :as set]))

(defn parse-field [field-str]
  (let [[_ name rules] (re-matches #"(.*?): (.*)" field-str)
        ranges (re-seq #"(\d+)-(\d+)" rules)]
    {:name   name
     :ranges (for [[_ & lowhigh] ranges]
               (mapv parse-int lowhigh))}))

(defn parse-ticket [ticket-str]
  (mapv parse-int (str/split ticket-str #",")))

(def input
  (let [input-str (read-input)
        [fields my-ticket nearby-tickets] (str/split input-str #"\n\n")
        fields (mapv parse-field (str/split-lines fields))
        nearby-tickets (mapv parse-ticket (drop 1 (str/split-lines nearby-tickets)))]
    {:fields         fields
     :my-ticket      (parse-ticket (second (str/split-lines my-ticket)))
     :nearby-tickets nearby-tickets}))

(defn test-range [value valid-ranges]
  (some
    (fn [[low high]]
      (<= low value high))
    valid-ranges))

(defn get-invalid-fields [fields ticket]
  (let [all-ranges (map :ranges fields)]
    (filter
      (fn [ticket-field]
        (not (some #(test-range ticket-field %) all-ranges)))
      ticket)))

(defn part1 []
  (let [{:keys [fields nearby-tickets]} input]
    (reduce + 0 (mapcat #(get-invalid-fields fields %) nearby-tickets))))

(defn eliminate-invalid-fields [all-values-for-field possible-fields]
  (filter
    (fn [field]
      (let [{ranges :ranges} field]
        (every? #(test-range % ranges) all-values-for-field)))
    possible-fields))

(defn claim-choices [possible-choices]
  (:claimed-choices
    (last
      (iterate-until-stable
        (fn [{:keys [possible-choices claimed-choices]}]
          (let [finished-choices (map #(if (= 1 (count %)) (first %)) possible-choices)
                claimed-choices (map #(or %1 %2) claimed-choices finished-choices)
                possible-choices (map
                                   #(set/difference % (set claimed-choices))
                                   possible-choices)]
            {:possible-choices possible-choices
             :claimed-choices  claimed-choices}))
        {:possible-choices (map set possible-choices)
         :claimed-choices  (vec (repeat (count possible-choices) nil))}))))

(defn part2 []
  (let [{:keys [fields nearby-tickets my-ticket]} input
        valid-nearby-tickets (filter #(empty? (get-invalid-fields fields %)) nearby-tickets)
        num-fields (count (first valid-nearby-tickets))
        all-values-per-field (map (fn [fidx] (map #(nth % fidx) valid-nearby-tickets)) (range num-fields))
        all-fields (set fields)
        possible-fields (repeat num-fields all-fields)
        allowed-fields (map
                         (fn [fields fidx]
                           (eliminate-invalid-fields (nth all-values-per-field fidx) fields))
                         possible-fields
                         (range))
        claimed-fields (claim-choices allowed-fields)
        relevant-field-indices (mapcat
                                 (fn [field fidx]
                                   (if (str/starts-with? (:name field) "departure")
                                     [fidx]))
                                 claimed-fields
                                 (range))]
    (reduce * (map #(get my-ticket %) relevant-field-indices))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
