(ns adventofcode2020.day13
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

(defn parse-input [input-str]
  (let [[line1 line2] (str/split-lines input-str)
        busses (for [entry (str/split line2 #",")]
                 (if (= entry "x")
                   :x
                   (parse-int entry)))]
    {:earliest (parse-int line1)
     :busses   busses}))

(def input
  (parse-input (read-input)))

;(def input (parse-input "939\n7,13,x,x,59,x,31,19"))

(defn part1 []
  (let [departure-time (:earliest input)
        [best-bus best-wait] (apply min-key second
                                    (for [bus (filter number? (:busses input))
                                          :let [earliest-time-for-bus (* (int (Math/ceil (/ departure-time bus))) bus)]]
                                      [bus (- earliest-time-for-bus departure-time)]))]
    (* best-bus best-wait)))

(defn find-common-phase-and-freq [x y]
  (let [{xphase :phase xfreq :freq} x
        {yphase :phase yfreq :freq ytarget-phase :target-phase} y
        common-freq (least-common-multiple xfreq yfreq)
        common-phase (first
                       (for [xmult (iterate (partial + xfreq) xphase)
                             :let [yrem (rem (+ xmult ytarget-phase (- yphase)) yfreq)]
                             :when (= 0 yrem)]
                         xmult))]
    {:phase common-phase :freq common-freq}))

(defn part2 []
  (let [phases-and-freqs (->> (map (fn [phase freq target-phase] {:phase phase :freq freq :target-phase target-phase}) (repeat 0) (:busses input) (range))
                              (filter (comp not #{:x} :freq)))
        {phase :phase} (reduce
                         find-common-phase-and-freq
                         phases-and-freqs)]
    phase))

(defn is-prime [n]
  (let [upper (Math/sqrt n)]
    (every?
      identity
      (for [v (range 2 (inc upper))]
        (not= 0 (rem n v))))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
