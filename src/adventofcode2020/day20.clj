(ns adventofcode2020.day20
  (:use adventofcode2020.advent-util)
  (:use clojure.pprint)
  (:require [clojure.string :as str]))

(defn parse-input [input-str]
  (for [tile-str (str/split input-str #"\n\n")]
    (let [[header-line & tile-lines] (str/split-lines tile-str)
          [_ tile-id] (re-matches #"Tile (\d+):" header-line)
          tile-id (parse-int tile-id)
          tile-data (vec tile-lines)]
      {:id   tile-id
       :data tile-data})))

(def input
  (parse-input (read-input)))

(defn get-edges [tile-data]
  [(first tile-data)
   (str/join (map last tile-data))
   (str/join (reverse (last tile-data)))
   (str/join (map first (reverse tile-data)))])

(defn flip-edge [edge]
  (str/join (reverse edge)))

(defn part1 []
  (let [augmented-tiles (map
                          (fn [tile]
                            (let [edges (get-edges (:data tile))]
                              (assoc tile
                                :edges edges
                                :all-edges (concat edges (map flip-edge edges)))))
                          input)
        all-edges (mapcat :all-edges augmented-tiles)
        edge-frequencies (frequencies all-edges)
        unpaired-edges (into #{} (map first (filter #(= 1 (second %)) edge-frequencies)))
        corner-pieces (filter #(= 2 (count (filter unpaired-edges (:edges %)))) augmented-tiles)]
    (reduce * (map :id corner-pieces))))

(defn assign-edges [tile]
  (let [{tile-data :data} tile
        top-edge (first tile-data)
        right-edge (str/join (map last tile-data))
        bottom-edge (last tile-data)
        left-edge (str/join (map first tile-data))]
    (assoc tile
      :top-edge top-edge
      :right-edge right-edge
      :bottom-edge bottom-edge
      :left-edge left-edge)))

(defn hflip-tile-data
  "Flips the tile data horizontally"
  [tile-data]
  (mapv #(str/join (reverse %)) tile-data))

(defn hflip-tile
  "Flips the tile horizontally"
  [augmented-tile]
  (let [{tile-data :data} augmented-tile
        updated-tile-data (hflip-tile-data tile-data)]
    (assign-edges (assoc augmented-tile :data updated-tile-data))))

(defn rotate-tile-data
  "Rotates the tile data 90 degrees clockwise"
  [tile-data]
  (apply
    mapv
    (fn [& column-data]
      (str/join (reverse column-data)))
    tile-data))

(defn rotate-tile
  "Rotates the tile 90 degrees clockwise"
  [augmented-tile]
  (let [{tile-data :data} augmented-tile
        updated-tile-data (rotate-tile-data tile-data)]
    (assign-edges (assoc augmented-tile :data updated-tile-data))))

(defn all-tile-data-variants
  "Finds all the rotated and flipped variants of the tile data"
  [tile-data]
  (let [rotates (take 4 (iterate rotate-tile-data tile-data))
        flips (take 4 (iterate rotate-tile-data (hflip-tile-data tile-data)))]
    (concat rotates flips)))

(defn all-tile-variants
  "Finds all the rotated and flipped variants of the tile"
  [tile]
  (let [init-tile (assign-edges tile)
        rotates (take 4 (iterate rotate-tile init-tile))
        flips (take 4 (iterate rotate-tile (hflip-tile init-tile)))]
    (concat rotates flips)))

(defn remove-tile-by-id [tiles id]
  (filter (comp not #{id} :id) tiles))

(defn remove-all-copies [tiles tile]
  (remove-tile-by-id tiles (:id tile)))

(def opposite-edges
  {:left-edge   :right-edge
   :right-edge  :left-edge
   :top-edge    :bottom-edge
   :bottom-edge :top-edge})

(defn generate-tile-sequence [init-tile available-tiles expand-edge]
  (let [match-edge (opposite-edges expand-edge)]
    (loop [available-tiles (remove-all-copies available-tiles init-tile)
           result [init-tile]]
      (let [edge-to-be-matched (expand-edge (last result))
            [matching-tile & candidate-tiles] (filter (comp #{edge-to-be-matched} match-edge) available-tiles)]
        (if (not (empty? candidate-tiles))
          (throw (IllegalStateException. "too many candidate tiles")))
        (if matching-tile
          (recur (remove-all-copies available-tiles matching-tile) (conj result matching-tile))
          {:seq result :available-tiles available-tiles})))))

(defn make-tile-grid [all-tiles]
  (let [all-edges (map :top-edge all-tiles)
        edge-frequencies (frequencies all-edges)
        unmatched-edges (set (for [[e c] edge-frequencies
                                   :when (odd? c)]
                               e))
        potential-corners (for [tile all-tiles
                                :when (unmatched-edges (:top-edge tile))
                                :when (unmatched-edges (:left-edge tile))]
                            tile)
        upper-left-corner (first potential-corners)
        {first-column :seq available-tiles :available-tiles} (generate-tile-sequence upper-left-corner all-tiles :bottom-edge)
        tile-grid (loop [available-tiles available-tiles
                         remaining first-column
                         result []]
                    (if-let [[first-element & remaining] (seq remaining)]
                      (let [{row :seq available-tiles :available-tiles} (generate-tile-sequence first-element available-tiles :right-edge)]
                        (recur available-tiles remaining (conj result row)))
                      result))]
    tile-grid))

(defn map-tile-grid [f tile-grid]
  (mapv
    (fn [row] (mapv f row))
    tile-grid))

(defn select-tile-data [tile-data y x h w]
  (mapv
    (fn [row]
      (subs row x (+ x w)))
    (subvec tile-data y (+ y h))))

(defn shrink-tile-data [tile-data]
  (select-tile-data tile-data 1 1 (- (count tile-data) 2) (- (count (first tile-data)) 2)))

(defn stitch-tile-grid [tile-grid]
  (let [shrunken-tile-data (map-tile-grid #(shrink-tile-data (:data %)) tile-grid)]
    (vec
      (mapcat
        (fn [tile-row]
          (let [lines (apply map vector tile-row)]
            (map
              (fn [segs] (str/join segs))
              lines)))
        shrunken-tile-data))))


(defn visualize-tile-grid [tile-grid]
  (let [tile-data (map-tile-grid :data tile-grid)]
    (str/join
      "\n\n"
      (for [row tile-data]
        (let [lines (apply map vector row)]
          (str/join
            "\n"
            (map
              #(str/join "   " %)
              lines)))))))

(def sea-monster-pattern (str/split-lines "                  # \n#    ##    ##    ###\n #  #  #  #  #  #   "))

(defn sea-monster-at [stitched y x]
  (let [smh (count sea-monster-pattern)
        smw (count (first sea-monster-pattern))]
    (reduce
      #(and %1 %2) true
      (for [sy (range smh)
            sx (range smw)]
        (let [sch (get-in stitched [(+ y sy) (+ x sx)])
              pch (get-in sea-monster-pattern [sy sx])]
          (cond
            (= pch \#)
            (= sch \#)

            :else
            true))))))

(defn count-sea-monsters [stitched]
  (count
    (let [h (count stitched)
          w (count (first stitched))]
      (for [y (range h)
            x (range w)
            :when (sea-monster-at stitched y x)]
        [y x]))))


(defn part2 []
  (let [all-tiles (mapcat all-tile-variants input)
        tile-grid (make-tile-grid all-tiles)
        stitched (stitch-tile-grid tile-grid)
        stitched-variants (all-tile-data-variants stitched)
        num-sea-monsters (apply max (map count-sea-monsters stitched-variants))
        num-hashes-per-sea-monster (count (filter #{\#} (str/join sea-monster-pattern)))
        num-hashes-in-stitched (count (filter #{\#} (str/join stitched)))]
    (- num-hashes-in-stitched (* num-sea-monsters num-hashes-per-sea-monster))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
