(ns adventofcode2020.day4
  (:require [clojure.string :as str])
  (:use adventofcode2020.advent-util))

(defn parse-single-passport [str]
  (into {} (for [[_ k v] (re-seq #"([^\s]+):([^\s]+)" str)]
             [(keyword k) v])))

(def input
  (let [input (read-input)]
    (for [chunk (str/split input #"\n\n")]
      (parse-single-passport chunk))))

(def part1-required-fields #{:byr :iyr :eyr :hgt :hcl :ecl :pid})

(defn check-year [min max]
  (fn [str]
    (if (re-find #"^\d{4}$" str)
      (let [parsed (parse-int str)]
        (<= min parsed max)))))

(def eye-colors
  #{"amb" "blu" "brn" "gry" "grn" "hzl" "oth"})

(def part2-validators
  {:byr (check-year 1920 2002)
   :iyr (check-year 2010 2020)
   :eyr (check-year 2020 2030)
   :hgt (fn [str]
          (if-let [[_ num unit] (re-find #"^(\d+)(cm|in)$" str)]
            (let [parsed-num (parse-int num)]
              (case unit
                "cm" (<= 150 parsed-num 193)
                "in" (<= 59 parsed-num 76)))))
   :hcl (fn [str]
          (re-find #"^#[0-9a-f]{6}$" str))
   :ecl (fn [str]
          (eye-colors str))
   :pid (fn [str]
          (re-find #"^\d{9}$" str))})

(def part2-validator
  (apply
    every-pred
    (for [[k v] part2-validators]
      (fn [passport]
        (let [str (k passport)]
          (and str
               (v str)))))))

(defn part1 []
  (count
    (for [passport input
          :when (every? (set (keys passport)) part1-required-fields)]
      passport)))

(defn part2 []
  (count
    (filter part2-validator input)))

(defn -main []
  (println (part1))
  (println (part2)))