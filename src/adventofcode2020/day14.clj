(ns adventofcode2020.day14
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

(defn parse-line [line]
  (let [[_ memaddr value] (re-matches #"(?:mem\[(\d+)\]|mask) = ([X\d]+)" line)]
    (if memaddr
      {:type  :mem
       :addr  (Long/parseLong memaddr 10)
       :value (Long/parseLong value 10)}
      {:type     :mask
       :mask-str value})))

(def input
  (parse-input-lines parse-line))

;(def input
;  (mapv parse-line (str/split-lines "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0")))

(defn set-mem-value-masked [state instr]
  (let [{:keys [addr value]} instr
        {mask-str :mask-str} state
        and-mask (Long/parseLong (str/replace mask-str "X" "1") 2)
        or-mask (Long/parseLong (str/replace mask-str "X" "0") 2)
        updated-value (-> value
                          (bit-and and-mask)
                          (bit-or or-mask))]
    (assoc-in state [:mem addr] updated-value)))

(defn set-mask [state instr]
  (let [{mask-str :mask-str} instr]
    (assoc state :mask-str mask-str)))

(defn part1 []
  (let [final-state (reduce
                      (fn [state instr]
                        (case (:type instr)
                          :mem (set-mem-value-masked state instr)
                          :mask (set-mask state instr)))
                      {:mask-str (apply str (repeat 36 "0")) :mem {}}
                      input)]
    (reduce + 0 (vals (:mem final-state)))))

(defn bit-put [x n v]
  (cast Boolean v)
  (if v
    (bit-set x n)
    (bit-clear x n)))

(defn generate-addr-masks [mask]
  (let [and-mask (parse-long (-> mask (str/replace "1" "X") (str/replace "0" "1") (str/replace "X" "0")) 2)
        base-or-mask (parse-long (str/replace mask "X" "0") 2)
        num-xs (count (filter #{\X} mask))
        generate-range (bit-shift-left 1 num-xs)
        shifts (for [[ch pos] (map vector (reverse mask) (range))
                     :when (= ch \X)]
                 pos)
        samples (for [[output-bit-pos sample-bit-pos] (map vector shifts (range))]
                  [sample-bit-pos output-bit-pos])
        or-masks (for [generated-bits (range generate-range)]
                   (reduce
                     (fn [mask [sample-pos output-pos]]
                       (bit-put mask output-pos (bit-test generated-bits sample-pos)))
                     base-or-mask
                     samples))]
    (for [or-mask or-masks]
      {:andmask and-mask
       :ormask  or-mask})))

(defn generate-addrs [mask-str base-addr]
  (let [and-mask (parse-long (-> mask-str (str/replace "1" "X") (str/replace "0" "1") (str/replace "X" "0")) 2)
        base-or-mask (parse-long (str/replace mask-str "X" "0") 2)
        num-xs (count (filter #{\X} mask-str))
        generate-range (bit-shift-left 1 num-xs)
        shifts (for [[ch pos] (map vector (reverse mask-str) (range))
                     :when (= ch \X)]
                 pos)
        samples (for [[output-bit-pos sample-bit-pos] (map vector shifts (range))]
                  [sample-bit-pos output-bit-pos])
        or-masks (for [generated-bits (range generate-range)]
                   (reduce
                     (fn [mask [sample-pos output-pos]]
                       (bit-put mask output-pos (bit-test generated-bits sample-pos)))
                     base-or-mask
                     samples))]
    (for [or-mask or-masks]
      (-> base-addr
          (bit-and and-mask)
          (bit-or or-mask)))))

(defn set-mem-value-multiaddr [state instr]
  (let [{mask-str :mask-str} state
        {:keys [addr value]} instr
        addresses (generate-addrs mask-str addr)]
    (reduce
      (fn [state address]
        (assoc-in state [:mem address] value))
      state
      addresses)))

(defn part2 []
  (let [final-state (reduce
                      (fn [state instr]
                        (case (:type instr)
                          :mem (set-mem-value-multiaddr state instr)
                          :mask (set-mask state instr)))
                      {:mask-str (apply str (repeat 36 "0")) :mem {}}
                      input)]
    (reduce + 0 (vals (:mem final-state)))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
