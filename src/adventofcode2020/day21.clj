(ns adventofcode2020.day21
  (:use adventofcode2020.advent-util)
  (:use clojure.pprint)
  (:require [clojure.string :as str])
  (:require [medley.core :as medley]
            [clojure.set :as set]))

(defn parse-input [input-str]
  (let [lines (str/split-lines input-str)]
    (for [line lines]
      (let [[_ ingredient-list allergen-list] (re-matches #"(.*) \(contains (.*)\)" line)
            ingredients (str/split ingredient-list #" ")
            allergens (str/split allergen-list #", ")]
        {:ingredients ingredients
         :allergens   allergens}))))

(def input
  (parse-input (read-input)))

;(def input
;  (parse-input "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)"))

(defn merge-allergen-ingredients [entries]
  (apply
    merge-with
    set/intersection
    (for [{:keys [ingredients allergens]} entries
          allergen allergens]
      {allergen (set ingredients)})))

(defn part1 []
  (let [all-ingredients (mapcat :ingredients input)
        ingredient-frequencies (frequencies all-ingredients)
        all-ingredients (set all-ingredients)
        ingredient-lists-by-allergen (merge-allergen-ingredients input)
        possibly-allergen-ingredients (apply set/union (vals ingredient-lists-by-allergen))
        impossible-ingredients (set/difference all-ingredients possibly-allergen-ingredients)]
    (reduce #(+ %1 (get ingredient-frequencies %2))
            0
            impossible-ingredients)))

(defn claim-ingredients-step [state]
  (let [{:keys [possible-allergen-ingredients claimed]} state
        new-claimed (->> possible-allergen-ingredients
                         (medley/filter-vals #(= 1 (count %)))
                         (medley/map-vals first))
        claimed (merge claimed new-claimed)
        possible-allergen-ingredients (medley/map-vals
                                        #(set/difference % (set (vals new-claimed)))
                                        possible-allergen-ingredients)]
    {:possible-allergen-ingredients possible-allergen-ingredients
     :claimed                       claimed}))

(defn claim-ingredients [possible-allergen-ingredients]
  (:claimed
    (last
      (iterate-until-stable
        claim-ingredients-step
        {:possible-allergen-ingredients possible-allergen-ingredients
         :claimed                       {}}))))

(defn part2 []
  (let [ingredient-lists-by-allergen (merge-allergen-ingredients input)
        claimed (into (sorted-map) (claim-ingredients ingredient-lists-by-allergen))]
    (str/join
      ","
      (for [[_ v] claimed]
        v))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
