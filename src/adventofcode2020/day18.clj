(ns adventofcode2020.day18
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]))

(defn read-number [s]
  (loop [idx 0]
    (cond
      (>= idx (count s)) [s ""]
      (not (Character/isDigit ^char (nth s idx))) [(subs s 0 idx) (subs s idx)]
      :else (recur (inc idx)))))

(def input
  (read-input-lines))

(defn tokenize [s]
  (lazy-seq
    (let [s (str/triml s)]
      (if (not (empty? s))
        (let [leading-ch (first s)]
          (cond
            (Character/isDigit ^char leading-ch)
            (let [[number s] (read-number s)]
              (cons (parse-int number) (tokenize s)))

            :else
            (cons leading-ch (tokenize (subs s 1)))))))))

(defn drop-last-vec [v]
  (subvec v 0 (dec (count v))))

(defn split-last-vec [v n]
  (let [end (- (count v) n)]
    [(subvec v 0 end) (subvec v end)]))

(defn convert-to-rpn [operator-priority tokens]
  (letfn [(convert-to-rpn-helper [tokens operator-stack]
            (lazy-seq
              (loop [tokens tokens
                     operator-stack operator-stack]
                (if-let [[token & rest-tokens] (seq tokens)]
                  (cond
                    (number? token)
                    (cons token (convert-to-rpn-helper rest-tokens operator-stack))

                    (= \( token)
                    (recur rest-tokens (conj operator-stack token))

                    (= \) token)
                    (if (= \( (last operator-stack))
                      (recur rest-tokens (drop-last-vec operator-stack))
                      (cons (last operator-stack) (convert-to-rpn-helper tokens (drop-last-vec operator-stack))))

                    (empty? operator-stack)
                    (recur rest-tokens (conj operator-stack token))

                    (= \( (last operator-stack))
                    (recur rest-tokens (conj operator-stack token))

                    :else
                    (let [top-op (last operator-stack)
                          top-prec (get operator-priority top-op)
                          inc-prec (get operator-priority token)]
                      (if (> inc-prec top-prec)
                        (recur rest-tokens (conj operator-stack token))
                        (cons (last operator-stack) (convert-to-rpn-helper tokens (drop-last-vec operator-stack))))))
                  (if (empty? operator-stack)
                    nil
                    (cons (last operator-stack) (convert-to-rpn-helper tokens (drop-last-vec operator-stack))))))))]
    (convert-to-rpn-helper tokens [])))

(defn evaluate-rpn [ops]
  (loop [stack []
         ops ops]
    (if-let [[op & ops] ops]
      (cond
        (number? op)
        (recur (conj stack op) ops)

        (= \+ op)
        (let [[stack [x y]] (split-last-vec stack 2)]
          (recur (conj stack (+ x y)) ops))

        (= \* op)
        (let [[stack [x y]] (split-last-vec stack 2)]
          (recur (conj stack (* x y)) ops)))
      (last stack))))

(defn evaluate-str-part1 [s]
  (let [tokens (tokenize s)]
    (evaluate-rpn (convert-to-rpn {\+ 1 \* 1} tokens))))

(defn part1 []
  (reduce + (map evaluate-str-part1 input)))

(defn evaluate-str-part2 [s]
  (let [tokens (tokenize s)]
    (evaluate-rpn (convert-to-rpn {\+ 2 \* 1} tokens))))

(defn part2 []
  (reduce + (map evaluate-str-part2 input)))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
