(ns adventofcode2020.day25
  (:use adventofcode2020.advent-util)
  (:use clojure.pprint)
  (:require [clojure.string :as str]))

(defn parse-input [input-str]
  (let [[card-public door-public] (map parse-long (str/split-lines input-str))]
    {:card-public card-public
     :door-public door-public}))

(def input
  (parse-input (read-input)))

;(def input (parse-input "5764801\n17807724"))

(defn transformation-rounds [subject-number]
  (iterate
    #(mod (* % subject-number) 20201227)
    1))


(defn part1 []
  (let [rounds (transformation-rounds 7)
        {:keys [card-public door-public]} input
        [card-loop-size door-loop-size] (loop [idx 0
                                               card-idx nil
                                               door-idx nil
                                               rounds rounds]
                                          (let [value (first rounds)]
                                            (cond
                                              (and card-idx door-idx)
                                              [card-idx door-idx]

                                              (and (nil? card-idx) (= value card-public))
                                              (recur (inc idx) idx door-idx (rest rounds))

                                              (and (nil? door-idx) (= value door-public))
                                              (recur (inc idx) card-idx idx (rest rounds))

                                              :else
                                              (recur (inc idx) card-idx door-idx (rest rounds)))))
        encryption-key (nth (transformation-rounds door-public) card-loop-size)]
    encryption-key))







(defn part2 [])

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
