(ns adventofcode2020.day9
  (:use adventofcode2020.advent-util)
  (:import (java.util Collections)))

(def input
  (vec (parse-input-lines #(bigint (BigInteger. ^String %1 10)))))

(defn check-candidates [candidates target]
  (let [sorted-candidates (vec (sort candidates))]
    (loop [current-candidates sorted-candidates]
      (if (<= (count current-candidates) 1)
        false
        (let [left (nth current-candidates 0)
              rest-candidates (subvec current-candidates 1)
              right (- target left)
              idx (Collections/binarySearch rest-candidates right compare)]
          (if (>= idx 0)
            true
            (recur rest-candidates)))))))

(defn part1 []
  (first
    (for [part (partition 26 1 input)
          :let [[candidates [target]] (split-at 25 part)]
          :when (not (check-candidates candidates target))]
      target)))

(defn part2 []
  (let [bad-number (part1)
        cumulative-sums (vec (reductions + 0 input))]
    (loop [left 0
           right 1]
      (cond
        (>= right (count cumulative-sums)) nil
        (< (- right left) 2) (recur left (inc right))
        :else (let [current-sum (- (cumulative-sums right) (cumulative-sums left))
                    comparison (compare current-sum bad-number)]
                (cond
                  (neg? comparison) (recur left (inc right))
                  (pos? comparison) (recur (inc left) right)
                  (zero? comparison) (let [index-range (range left right)
                                           inputs-in-range (vec (sort (map input index-range)))]
                                       (+ (first inputs-in-range) (last inputs-in-range)))))))))

(defn -main []
  (println (str (part1)))
  (println (str (part2))))
