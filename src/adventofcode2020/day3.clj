(ns adventofcode2020.day3
  (:use adventofcode2020.advent-util))

(def input
  (parse-input-lines vec))

(defn translate-coordinate [[x y]]
  [(mod x (count (first input))) y])

(defn sample-at [[x y]]
  (let [[x y] (translate-coordinate [x y])]
    (nth (nth input y) x)))

(defn slope-coords [[x y] [dx dy]]
  (take-while
    (fn [[_ y]] (< y (count input)))
    (iterate
      (partial map + [dx dy])
      [x y])))

(defn count-collisions [slope]
  (count
    (for [c (slope-coords [0 0] slope)
          :let [ch (sample-at c)]
          :when (= ch \#)]
      ch)))

(defn part1 []
  (count-collisions [3 1]))

(def slopes
  [[1 1] [3 1] [5 1] [7 1] [1 2]])

(defn part2 []
  (reduce
    *
    (map count-collisions slopes)))

(defn -main []
  (println (part1))
  (println (part2)))
