(ns adventofcode2020.day6
  (:use adventofcode2020.advent-util)
  (:require [clojure.string :as str]
            [clojure.set :as set]))

(def input
  (let [chunks (str/split (read-input) #"\n\n")]
    (for [chunk chunks]
      (for [line (str/split-lines chunk)]
        (set line)))))

(defn part1 []
  (reduce
    +
    (for [party input]
      (count (reduce into #{} party)))))

(defn part2 []
  (reduce
    +
    (for [party input]
      (count (apply set/intersection party)))))

(defn -main []
  (println (part1))
  (println (part2)))