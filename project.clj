(defproject adventofcode2020 "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [medley "1.3.0"]]
  :repl-options {:init-ns adventofcode2020.day23})
